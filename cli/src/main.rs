use std::{io, time::Duration};

use clap::{Command, CommandFactory, Parser, Subcommand};
use clap_complete::generate;
use libserial2midi::{self, list_devices, start_virtual_midi};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None, name="serial2midi")]
struct Cli {
    /// Set the time in ms that the cpu sleeps between iterations
    #[arg(short, long, default_value_t = 15)]
    latency: u64,
    /// Set the baud rate that shall get used for the serial ports
    #[arg(short = 'b', long, default_value_t = 115_200)]
    baud_rate: u32,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Midify {
        /// Expression to match serial devices against. First that evaluates to true will get MIDIfied.
        #[arg(short = 'm', long = "match", requires = "name")]
        match_expression: String,

        /// Name for the midi device
        #[arg(short = 'n', long, requires = "match_expression")]
        name: String,
    },
    List,
    PrintCompletions {
        #[arg(value_name = "shell")]
        shell: clap_complete::Shell,
    },
}

fn main() {
    tracing_subscriber::fmt::init();

    let cli = Cli::parse();
    let latency = Duration::from_millis(cli.latency);

    match &cli.command {
        Commands::List => {
            let devices = list_devices(Duration::from_secs(3), cli.baud_rate).unwrap();
            if devices.is_empty() {
                println!("No devices found :/");
                return;
            }
            for device in devices {
                println!("\n{}", device)
            }
        }
        Commands::Midify {
            match_expression,
            name,
        } => loop {
            if let Err(e) = start_virtual_midi(name, match_expression, cli.baud_rate, latency) {
                println!("Unexpected error. Please report. {}", e);
            }
        },
        Commands::PrintCompletions { shell } => {
            let mut command: Command = Cli::command();
            let app_name = &command.get_name().to_string();
            generate(shell.to_owned(), &mut command, app_name, &mut io::stdout());
        }
    }
}
